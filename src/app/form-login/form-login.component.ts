import { Component } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.scss']
})
export class FormLoginComponent {
constructor(private router: Router,private authService: AuthService){

}

  Login(Username:string, Password:string){
    if(this.authService.login(Username.trim(),Password.trim())){
      Swal.fire({
            icon:'success',
            title: 'Success',
            text: 'Login Successful'
          }).then(()=>{
            this.router.navigate(['/dashboard'])
          })
    }
    // if(Username == "" || Password == ""){
    //   Swal.fire({
    //     icon: 'error',
    //     title: 'Oops...',
    //     text: 'กรุณาป้อนข้อมูลให้ครบ'
    //   })
    // }else if(Username !== 'Jjpj42' || Password !== '1234'){
    //   Swal.fire({
    //     icon: 'error',
    //     title: 'Oops...',
    //     text: 'Username or Password Incorrect'
    //   })
    // }else{
    //   Swal.fire({
    //     icon:'success',
    //     title: 'Success',
    //     text: 'Login Successful'
    //   }).then(()=>{
    //     this.router.navigate(['/dashboard'])
    //   })
    // }

  }
  
}
