import { Component } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  constructor(private router: Router,private authService: AuthService){
  this.showLoading();
  }
  items: MenuItem[] = [];
  activeItem!: MenuItem;

  ngOnInit() {
      this.items = [
          { label: 'Home', icon: 'pi pi-fw pi-home' },
          { label: 'Calendar', icon: 'pi pi-fw pi-calendar' },
          { label: 'Edit', icon: 'pi pi-fw pi-pencil' },
          { label: 'Documentation', icon: 'pi pi-fw pi-file' },
          { label: 'Settings', icon: 'pi pi-fw pi-cog' }
      ];
      this.activeItem = this.items[0];
  }
  loading = false;
  async showLoading() {
    this.loading = true;
    await new Promise<void>(resolve => setTimeout(() => resolve(), 2000 ));
    this.loading = false;
  }
  Logout(){
    if(this.authService.logout()){
      this.router.navigate(['/login']);
    }
  }

}
