import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormLoginComponent } from './form-login/form-login.component';
import { AuthGuard } from './auth.guard';
const routes: Routes = [
  { path: '',redirectTo:'login', pathMatch: 'full'},
  { path: 'login', component: FormLoginComponent },
  { path: 'dashboard', component: DashboardComponent ,canActivate: [AuthGuard]}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
