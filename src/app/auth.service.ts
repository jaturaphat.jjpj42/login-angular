import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor() { }

  isLoggedIn(): boolean {
    // ตรวจสอบว่ามี token ของการเข้าสู่ระบบอยู่หรือไม่
    return localStorage.getItem('token') !== null;
  }

  login(username: string, password: string): boolean {
    // ตรวจสอบ username และ password ในระบบ
    if (username === 'Jjpj42' && password === '1234') {
      // สร้าง token สำหรับการเข้าสู่ระบบ
      localStorage.setItem('token', 'access_token');
      return true;
    } else {
      Swal.fire({
        icon: 'error',
        title: 'ผิดพลาด',
        text: 'คุณเข้าสู่ระบบไม่ถูกต้อง'
      });
      return false;
    }
  }

  logout(): boolean {
    // ลบ token ออกจากระบบ
    Swal.fire({
      icon: 'success',
        title: 'complete',
        text: 'คุณออกจากระบบเรียบร้อย'
    })
    localStorage.removeItem('token');
    return true;
  }
  
}
